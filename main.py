from flask import Flask, render_template, request
from urllib import parse
import apis
import foursquare

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
  city = '東京都品川区平塚'
  if request.method == 'POST':
    city = request.form['keyword']
  lat,lng = apis.address_to_latlng(city)
  forecast = apis.get_weather(lat, lng)
  shopinfo = foursquare.get_id("35.658517,139.701334", "cafe")
  shopname = shopinfo
  return render_template('index.html', city=city, lat=lat, lng=lng, forecast=forecast, shopname=shopname )



if __name__ == '__main__':
  app.run(host='0.0.0.0')
